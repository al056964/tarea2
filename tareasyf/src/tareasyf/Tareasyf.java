/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tareasyf;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Scanner;
/**
 *
 * @author Omar Rodriguez
 */
public class Tareasyf {

    /**
     * Clase : imprimirMsj Parámetros : String sMensaje Acción : Imprime el
     * mensaje y hace un salto de línea
     */
    static void imprimirMsj(String sMensaje) {
        System.out.println(sMensaje);
    }

    /**
     * Clase : imprimirM Parámetros : String sMen Acción : Imprime el mensaje en
     * la misma línea
     */
    static void imprimirM(String sMen) {
        System.out.print(sMen);
    }

    /**
     * Clase : separador 
     * Parámetros : 
     * Acción : Crea un separador
     */
    static void separador() {
        imprimirMsj("-------------------------------");
    }

    /**
     * Clase : encabezado 
     * Parámetros : 
     * Acción : Muestra información sobre el
     * programa
     */
    static void encabezado() {
        imprimirMsj("Universidad Autónoma de Campeche");
        imprimirMsj("Facultad de Ingeniería");
        imprimirMsj("Ingeniería en Sistemas Computacionales");
        imprimirMsj("Omar Jasiel Rodriguez Cab | 56964");
        separador();
    }
    
    //Ejericio switch 2
    static void semana(){
        Scanner sc = new Scanner(System.in);
        imprimirM("Introduzca 1 para semana inglesa y 0 para semana laborable: ");
        int is = sc.nextInt();
        boolean semana = is == 1;       
        imprimirM("Introduzca un número del 1 al 7, correspondiente al diá que desea saber: ");
        int num = sc.nextInt();
        if (semana){
            switch (num){
            case 1: 
                imprimirMsj("Domingo");
                break;
            case 2:
                imprimirMsj("Lunes");  
                break;
            case 3:
                imprimirMsj("Martes");
                break;
            case 4:
                imprimirMsj("Miércoles");
                break;
            case 5:
                imprimirMsj("Jueves");
                break;
            case 6:
                imprimirMsj("Viernes");
                break;
            case 7:
                imprimirMsj("Sábado");
                break;
            default: 
                imprimirMsj("Día no válido");
                break;
        }
        }
        else{
            switch(num){
               case 1: 
                imprimirMsj("Lunes");
                break;
            case 2:
                imprimirMsj("Martes");  
                break;
            case 3:
                imprimirMsj("Miércoles");
                break;
            case 4:
                imprimirMsj("Jueves");
                break;
            case 5:
                imprimirMsj("Viernes");
                break;
            case 6:
                imprimirMsj("Sábado");
                break;
            case 7:
                imprimirMsj("Domingo");
                break;
            default: 
                imprimirMsj("Día no válido");
                break; 
            }
        }
    }
       
    //Ejericio for 5
    static void nnumeros() {
        InputStreamReader isr=new InputStreamReader (System.in);
        BufferedReader br=new BufferedReader (isr);
        Scanner sc = new Scanner(System.in);
        
        double suma = 0;
        double iMayor = 0;
        double iMenor = 10000;
        imprimirM("Introducir el total de números: ");
        double iN = sc.nextInt();
        for (int i = 1; i <= iN; i++) {
                        System.out.println("Ingrese número " + i+": ");
            double iNum = sc.nextDouble();
            if (iNum > iMayor)
            {
                iMayor= iNum;

            }
            if (iNum < iMenor)
            {
                iMenor = iNum;
            }
            suma = suma + iNum;
        }
        double prom = suma / iN;
        imprimirMsj("La suma de los números es de: " + suma);
        imprimirMsj("El promedio de los números es de: " + prom);
        imprimirMsj("El número menor es: " + iMenor);
        imprimirMsj("El número mayor es: " + iMayor);
        double iDif = iMayor - iMenor;
        imprimirMsj("La diferencia entre ambos es de: " + iDif);
        separador();
    }

        //Ejericicio for 6
    static void diassemana() {
        String[] dias = {"Domingo", "Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado"};
        for (int i=0; i<=6; i++){
            imprimirMsj(dias[i]);
        }
    }

    /**
     * Clase : submenu 
     * Parámetros : iOpc 
     * Acción : Arroja el programa que se selecciona en el menú
     */
    static void submenu(int iOpc) {
        if (iOpc == 1) {
            imprimirMsj("Tipo de semana");
            separador();
            semana();
        } else {
            if (iOpc == 2) {
                imprimirMsj("Suma, promedio, mayor y menor de n números");
                separador();
                nnumeros();
            }
            else {
                if (iOpc == 3) {
                    imprimirMsj("Arreglo de los días de la semana");
                    separador();
                    diassemana();
                }
                else{
                    if (iOpc == 4){
                    imprimirMsj("Gracias por usar la aplicación");
                    separador();  
                    }
                    else{
                        imprimirMsj("Opción inválida");
                    }
                }

            }
        }
    }

    /**
     * Clase : menu 
     * Parámetros : 
     * Acción : Escoge un programa de acuerdo al número introducido
     */
    static void menu() {
        imprimirMsj("MENÚ.");
        imprimirMsj("1.- Crear una método que lea un número de tipo int y se solicite a otro método que valide el dato entre el 1 al 7 si el valor esta fuera de este rango regrese que es incorrecto y el nombre del día de la semana, como segundo parametro reciba una valor boleano donde true es semana inglesa (inicia domingo), false es semana laborable (inicia en lunes).");
        imprimirMsj("2.- Pedir un número y leer n veces números, realizar la suma de los numeros, sacar el promedio y determinar cual número introducido es el mayor y cual el menor, y la distancia númerica entre ellos.");
        imprimirMsj("3.- Imprima un arreglo que tenga los nombres de los días de la semana.");
        imprimirMsj("4.- Salir.");
        separador();
        Scanner sc = new Scanner(System.in);
        imprimirM("Teclee la opción deseada: ");
        int iOpc = sc.nextInt();
        submenu(iOpc);
    }
    
    public static void main(String[] args) {
        // TODO code application logic here
        encabezado();
        menu();
    }
    
}
